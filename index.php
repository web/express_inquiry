<?php
/**
 * 汽车之家车型大全类库API 离线版
 * @Copyright (C) 2019 比物福API All rights reserved.
 * @License http://tool.bitefu.com
 * @Author xiaogg <xiaogg@sina.cn>
 */
$getdata=$_REQUEST;if(empty($getdata))$getdata=$_POST?$_POST:$_GET;
if(empty($getdata['sn']) || empty($getdata['code'])){
header("content-Type: text/html; charset=utf-8");
echo '<html><head><title>免费快递查询api</title></head><body>参数 ?sn=快递单号&code=快递公司代码(kuaidi100)&type=0 json 1 url 2 html<br />支持get 和 post 请求';return;}
error_reporting("E_ALL");ini_set("display_errors", 1);header("content-Type: application/json; charset=utf-8");
require_once("include/function.php");
require_once("include/Kuaidi100.class.php");
$postdata=array_map('htmlspecialchars',$getdata);
if(empty($postdata['sn']) || empty($postdata['code']))$result=array('status'=>0,'info'=>'参数错误');
else{
    $kd=new Kuaidi100();
    $result=$kd->search($postdata);
}
ajaxReturn($result);
?>